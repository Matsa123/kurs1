window.onload = function () {

    const xhr = new XMLHttpRequest();
    const usd = document.querySelector(".usd");
    const eur = document.querySelector(".eur");
    const gbp = document.querySelector(".gbp");
    const rub = document.querySelector(".rub");
    const pln = document.querySelector(".pln");
    const date = document.querySelector(".date");

   
        xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
        
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                // JSON.parse - преобразование в объект строки полученной с сервера.
                var data = JSON.parse(xhr.responseText);

                date.innerHTML += data[0].exchangedate;

                data.forEach(i => {
                    if (i.cc == "USD") {
                        usd.innerHTML = i.rate.toFixed(2) + " грн";  
                    }
                    if (i.cc == "EUR") {
                        eur.innerHTML = i.rate.toFixed(2) + " грн";  
                    }
                    if (i.cc == "GBP") {
                        gbp.innerHTML = i.rate.toFixed(2) + " грн";  
                    }
                    if (i.cc == "RUB") {
                        rub.innerHTML = i.rate.toFixed(2) + " грн";  
                    }
                    if (i.cc == "PLN") {
                        pln.innerHTML = i.rate.toFixed(2) + " грн";  
                    }
                    
                });
               
            }
        }
        
        xhr.send();
    
}
